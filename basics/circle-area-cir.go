package main

import "fmt"

func main() {
	fmt.Println("Enter the radius of the circle:")
	foo()
	var r float64
	fmt.Scan(&r)
	const pi float32 = 3.14
	area := pi * r * r
	cir := 2 * pi * r
	fmt.Println("The area of circle is:", area, "\nThe circumference of the circle is:", cir)
	foo()
}


func foo() {
	fmt.Println("func foo called")
}
